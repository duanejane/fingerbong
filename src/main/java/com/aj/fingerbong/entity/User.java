package com.aj.fingerbong.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * (User)实体类
 *
 * @author makejava
 * @since 2020-05-22 14:05:50
 */
@Data
public class User implements Serializable {
    private static final long serialVersionUID = -81527796072863632L;
    
    private Long id;
    /**
    * 登录账号
    */
    private String username;
    
    private String nickname;
    
    private String icon;
    
    private String phone;
    
    private Integer age;
    
    private Integer gender;


}