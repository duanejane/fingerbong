package com.aj.fingerbong.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * (Relationship)实体类
 *
 * @author makejava
 * @since 2020-05-22 14:05:47
 */
@Data
public class Relationship implements Serializable {
    private static final long serialVersionUID = 964193125946414673L;
    
    private Long userId;
    
    private Long friendId;

}