package com.aj.fingerbong.dao;

import com.aj.fingerbong.entity.Relationship;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * (Relationship)表数据库访问层
 *
 * @author makejava
 * @since 2020-05-22 14:05:47
 */
@Mapper
public interface RelationshipDao {



    /**
     * 通过实体作为筛选条件查询
     *
     * @param userId 用户ID
     * @return 对象列表
     */
    List<Relationship> listRelationships(Long userId);

    /**
     * 新增数据
     *
     * @param relationship 实例对象
     * @return 影响行数
     */
    int insertRelationship(Relationship relationship);

    /**
     * 通过主键删除数据
     *
     * @param both 是否同时删除 1 是 0 否
     * @return 影响行数
     */
    int deleteRelationship(@Param("userId") Long userId,@Param("friendId")Long friendId,@Param("both")Integer both);

}