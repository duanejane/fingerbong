package com.aj.fingerbong.response;

import com.aj.fingerbong.enums.SuccessFailureEnum;
import lombok.Builder;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

@Data
@Builder
public class Response {
    @Builder.Default
    private Integer code;
    @Builder.Default
    private String msg;
    @Builder.Default
    private Object data;

    public Response(Integer code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static Response sucess(String msg, Object data) {
        return Response.builder().code(SuccessFailureEnum.SUCESS.getCode())
                .msg(StringUtils.isNotBlank(msg) ? msg : "sucess").data(data).build();
    }

    public static Response sucess(Object data) {
        return Response.builder().code(SuccessFailureEnum.SUCESS.getCode())
                .msg("sucess").data(data).build();
    }
    public static Response sucess() {
        return Response.builder().code(SuccessFailureEnum.SUCESS.getCode())
                .msg("sucess").build();
    }

    public static Response error(Integer code, String msg) {
        return Response.builder().code(code)
                .msg(StringUtils.isNotBlank(msg) ? msg : "error").build();
    }
}
