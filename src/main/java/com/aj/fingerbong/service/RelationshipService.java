package com.aj.fingerbong.service;

import com.aj.fingerbong.entity.Relationship;
import java.util.List;

/**
 * (Relationship)表服务接口
 *
 * @author makejava
 * @since 2020-05-22 14:05:47
 */
public interface RelationshipService {



    /**
     * 查询多条数据
     *
     * @param userId 用户ID
     * @return 对象列表
     */
    List<Relationship> listRelationships(Long userId);

    /**
     * 新增数据
     *
     * @param relationship 实例对象
     * @return 实例对象
     */
    Relationship insertRelationship(Relationship relationship);

    /**
     * 删除彼此的关系
     * @param userId
     * @param friendId
     * @param both
     */
   void deleteRelationship(Long userId,Long friendId,Integer both);
}