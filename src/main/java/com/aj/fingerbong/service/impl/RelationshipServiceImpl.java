package com.aj.fingerbong.service.impl;

import com.aj.fingerbong.dao.RelationshipDao;
import com.aj.fingerbong.entity.Relationship;
import com.aj.fingerbong.service.RelationshipService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Relationship)表服务实现类
 *
 * @author makejava
 * @since 2020-05-22 14:05:47
 */
@Service
public class RelationshipServiceImpl implements RelationshipService {
    @Resource
    private RelationshipDao relationshipDao;


    @Override
    public List<Relationship> listRelationships(Long userId) {
        return relationshipDao.listRelationships(userId);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Relationship insertRelationship(Relationship relationship) {
         relationshipDao.insertRelationship(relationship);
         return relationship;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deleteRelationship(Long userId, Long friendId, Integer both) {
        relationshipDao.deleteRelationship(userId ,friendId ,both );
    }
}