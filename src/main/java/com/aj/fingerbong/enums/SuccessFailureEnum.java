package com.aj.fingerbong.enums;

import java.io.Serializable;

public enum SuccessFailureEnum implements Serializable {
    SUCESS(1000,"操作成功"),
    ERROR(1001,"操作失败"),
    INTERNAL_SERVER_ERROR(1002,"服务器内部错误"),
    MISSING_PARAMETERS(1003,"缺少参数"),
    PASSWORD_INCORRECT(1004,"密码错误");

    SuccessFailureEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private Integer code;
    private String msg;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
