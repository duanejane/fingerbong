package com.aj.fingerbong.controller;

import com.aj.fingerbong.entity.User;
import com.aj.fingerbong.response.Response;
import com.aj.fingerbong.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * (User)表控制层
 *
 * @author makejava
 * @since 2020-05-22 14:05:50
 */
@RestController
@RequestMapping("/user")
public class UserController {
    /**
     * 服务对象
     */
    @Autowired
    private UserService userService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/one")
    public Response selectOne(Long id) {
        User user = this.userService.queryById(id);
        return Response.sucess(user);
    }


    @GetMapping("/friends")
    public Response listFriends(Long id) {
        List<User> users = this.userService.listFriends(id);
        return Response.sucess(users);
    }
}