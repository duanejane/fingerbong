package com.aj.fingerbong;

import com.aj.fingerbong.netty.chat.ChatServer;
import com.aj.fingerbong.netty.transport.TransportServer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

@Component
public class NettyBootStrap implements ApplicationListener<ContextRefreshedEvent> {
    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        ApplicationContext context = contextRefreshedEvent.getApplicationContext().getParent();
        if(ObjectUtils.isEmpty(context)){
            try {
                TransportServer.getInstance().start();
                ChatServer.getInstance().start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
