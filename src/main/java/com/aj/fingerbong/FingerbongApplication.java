package com.aj.fingerbong;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@MapperScan("com.aj.fingerbong.*")
public class FingerbongApplication {

    public static void main(String[] args) {
        SpringApplication.run(FingerbongApplication.class, args);
    }

}
